# 个人简历

---

#### 基础信息

<!-- <img style="width:190px" src="static/png/hmbb2.jpeg"> -->

-  姓名 ：胡康

-  性别 ：男

-  年龄 ：<span id="age">26</span>岁

-  电话 ：15773594340

-  所在城市 ：湖南长沙

<!-- -  期望薪资 ：面谈 -->

-  岗位 ：前端开发工程师


-  邮箱 : &nbsp; 250348426@qq.com


---

#### 专业技能

-  掌握 HTML5 CSS3 JavaScript Jquery
-  熟练使用 Vue2 以及全家桶,初步掌握 Vue3
-  熟练使用 Uni-app 进行的多端开发
-  熟悉 Vant elementUI uviewUI iviewUI 等UI框架
-  了解后台一些语言 如 PHP，Java，NodeJS 数据库 MySql
---

#### 工作经验

<details>
<summary>北海石基信息技术有限公司（2021-6 至今）</summary>
<pre>
<code>在职时间 2021-6 至今
石基信息 SDP 研发部
</code></pre>
</details>

<br>

<details>
<summary>湖南易趣在线旅游服务有限公司（2019-9 至 2021-06）</summary>
<pre>
<code>在职时间 2019-9 至 2021-06
文旅相关开发
</code></pre>
</details>

<br>

<details>
<summary>湖南微金塾网络科技有限公司（2019-5 至 2019-8）</summary>
<pre>
<code>在职时间 2019-5 至 2019-8
现已改名 长沙微金塾网络科技有限公司
金融相关开发
</code></pre>
</details>

<br>

<details>
<summary>湖南和永信息技术有限公司（2017-12 至 2019-03）</summary>
<pre>
<code>在职时间 2017-12 至 2019-03
2017-12 至 2018-3 为在校实习期
从事教育相关开发
</code></pre>
</details>

---

#### 教育背景

时间：2015-09 - 2018-06

学校：[湖南软件职业技术大学](https://baike.baidu.com/item/%E6%B9%96%E5%8D%97%E8%BD%AF%E4%BB%B6%E8%81%8C%E4%B8%9A%E6%8A%80%E6%9C%AF%E5%A4%A7%E5%AD%A6) - 软件开发与信息工程系 &nbsp;  |  &nbsp; 专业：软件技术

时间：2012-09 - 2015-06

学校：湖南郴州市三中

时间：2009-09 - 2012-06

学校：湖南郴州市五中

---

#### 项目经历

`从上至下 由新到旧 ,类似的项目不加以赘述`

- #### SDI-ETL

一款类似kettle的ETL 工具

<img src="static/project/sj/WechatIMG17.png"/>

<img src="static/project/sj/WechatIMG18.png"/>

<img src="static/project/sj/WechatIMG19.png"/>

##### 难点处理：

- 数据组装，数据回显
- 运行结果及时响应处理
- 多线程操作

---

- ####  infrasys ER

<img src="static/project/sj/WechatIMG16.png"/>

<img src="static/project/sj/WechatIMG15.png"/>

##### 技术涵盖

- 涉及多主题(elementUI 主题配置)，多语言（vue-i18n）
- 图表操作(AntV-G6) 数据可视化（echarts）等

[infrasys ER 介绍](https://www.shijigroup.cn/brands/infrasys-enterprisesolution.html)

---

- #####  锦泰安防（小程序）

```
技术栈 : 使用 uni-app 和 uview 构建的微信小程序

难点: 针对商品规格所设置的Sku通过笛卡尔积算法的存储和前端展示

描述: 打造线上电商，涉及 砍价，拼团，抢购，分销，积分商城等 五一假期成交量高达32万

微信搜索锦泰安防即可查看
```

<!-- `如下图所示 :`

<img style="margin:10px;" class="phone" src="static/project/jt/3.jpg"/>
<img style="margin:10px;" class="phone" src="static/project/jt/2.jpg"/>
<img class="phone" src="static/project/jt/4.jpg"/>
<img style="margin:10px;" class="phone" src="static/project/jt/1.jpg"/> -->

- ##### 一部手机游潇湘平台


```
1. 一部手机游潇湘(H5,公众号)

项目描述 : 对一部手机游潇湘1.0版本(Angular)进行的升级和重构(重构成Vue)

技术栈 : 采用 Vue2全家桶 + vant UI + weixin.js

总结 : 在Angular的版本进行重构的过程中不得不学习Angular从而了解业务逻辑，学习到了Angular的一些基础，同时也更加熟练的使用Vue

网址 : http://yxx.h-etrip.com/app

```

`如下图所示 :`

<img class="phone" src="static/project/jt/7.jpg"/>

<img class="phone" src="static/project/jt/11.jpg"/>

<img class="phone" src="static/project/jt/12.jpg"/> -->

 ```

2. 一部手机游潇湘(App)

项目描述 : 基于 uni-app 所开发的 App

技术栈 : uni-app 为主体配合 NativeJS 进行开发

总结 ：熟悉了uni-app 的 app 端开发和配置
```

```

3. 一部手机游潇湘后台管理系统

项目描述 : 使用 vue-element-admin 搭建的后台管理系统

工作内容 : 负责前端页面构建 采用 vue-element-admin 自带的 element UI ，数据的增删改查

工作总结 : vue-element-admin 是一个非常好项目案例特别是在权限判断路到由动态加载再到生成菜单栏的处理方法非常值得学习

个人仓库 : https://nidhoggdjoking.github.io/BackstageTemplate
```


```
4. 游潇湘支付宝小程序 1.0

项目描述 : 和阿里合作的项目，支付宝联合游潇湘打造湖南景区平台 实现景区门票线上预约

工作内容 : 后台产生票务和景区过一道阿里提供的接口数据和他们达成标椎，前端则使用他们提供的打卡，卡券等

工作难点 : 阿里提供的接口和前端组件全是现写的出现问题也没办法百度，异地联调许多不便而且他们交流只用钉钉
```

`流程图如下 :`

<img style="margin:10px;" class="phone" src="static/project/jt/5.jpg"/>

<img style="margin:10px;" class="phone" src="static/project/jt/9.jpg"/>

<img style="margin:10px;" class="phone" src="static/project/jt/8.jpg"/>

<img style="margin:10px;" class="phone" src="static/project/jt/10.jpg"/>

- ##### 智游系列

```
1. 智游 XX 小程序

项目概述 : 开发游潇湘平台下面的子项目 包括 : 智游衡阳县, 智游新邵, 智游祁阳, 智游汝城, 智游衡南 等小程序

工作总结 : 全部采用同一套API接口和相似的业务逻辑 ,除了不同UI设计图的以外项目结构大致相同 并未对项目经验提供太大的帮助

```

```
2. 智游系列后台

项目描述 :  首次使用若依框架-使用的前端技术Vue、Element后端SpringBoot & Security完全分离的权限管理系统

若依管理系统 : http://doc.ruoyi.vip
```

`如下图所示 :`

<img class="phone" src="static/project/jt/13.jpg"/>

<img class="phone" src="static/project/jt/6.jpg"/>




- ##### 点点管家2.0

```
描述 : 金融类应用通过其他支付平台提供的接口进行套现和智能还款的功能

技术栈 : 微信小程序开发

工作内容 : 主要负责对接接口以及使用百度大脑第三方API实现读取身份证照片信息，银行卡照片信息和银行卡四要素验证等

```

- #####  至真健康商城

```
项目描述 : 一个简单的微信公众号的商城，完成基本的商品展示与购买

技术栈 : 采用thinkPHP5模板渲染方式 HTML + CSS + JS +JQuery 语法类似 Svelte

项目总结 : 开始接触PHP主流框架因为非前后端分离所以也试着写过部分PHP业务代码,增加了对thinkPHP框架的了解
```


- ##### 湖南和永信息技术有限公司官网(全栈开发)

```
前端技术栈: template-web + Jquery + amazeui

后端技术栈 ：PHP + MySql

描述 ：使用公司内部研制一套PHP框架独自完成官网和后台系统的前后端开发，从而了解些后台的架构以及掌握SQL的基础语句

总结 : 首个独自完成前后端的项目，对项目的开发的全部流程都有了一定的了解

网址:http://www.hyxxit.com

后台管理 : http://www.hyxxit.com/manage/index.html  测试账号：joking / 123
```


- ##### 和永教育平台（和永教育培训网）


```
和永教育平台(web端)

技术栈: JQuery + Bootstrap + art-template 模板引擎渲染

网址 ：http://edu.hyxxit.com
```

```
和永教育培训网(响应式)

技术栈: JQuery + art-template 模板引擎渲染

难点 : 移动端与PC端的响应式布局多套CSS媒体判断 和 考试系统的数据展示与功能

网址 ：http://edu.hyxxit.com/study

测试账号 : 15773594340 / 123456

```

```
和永教育App

技术栈 : 纯 Android 开发

描述 : 使用Eclipse工具进行的Android原生开发，也是由Java转前端的契机

难点 ：Android 的布局是一套独立的语法对没接触过的东西刚上手比较吃力，至此开始加入前端

下载地址 : http://edu.hyxxit.com/app.html
```



####  代码学习仓库


?> [Gitee](https://gitee.com/NidhoggDJoking) &nbsp; 存放个人学习案例 可查阅代码风格 

```
[发布的NPM包](https://www.npmjs.com/package/joking-navigation)

[基于serverless模式的uniCloud开发尝鲜](https://gitee.com/NidhoggDJoking/juni-cloud)

[UniApp + NativeJS APP Demo](https://gitee.com/NidhoggDJoking/uand)

[学习React中的JSX和函数式编程思想](https://gitee.com/NidhoggDJoking/React)

[基于Express的NODE](https://gitee.com/NidhoggDJoking/node)

[Vue3](https://gitee.com/NidhoggDJoking/vuenext)

[svelte](https://gitee.com/NidhoggDJoking/svelte)

[ThinkPHPv6](https://gitee.com/NidhoggDJoking/ThinkPHPv6)

[Angular学习](https://github.com/NidhoggDJoking/Angular)

```

#### 感谢看到最后

?> 这里其实是我个人的一个博客 可点击左下角打开菜单栏 

<img style="width:500px;margin:10px;" src="static/project/this/local.png"/>

<img style="width:500px;margin:10px;" src="static/project/this/sw.png"/>

在第一次加载时使用了 `Service Workers`  和 路由的本地存储 以便于之后的顺畅以及断网情况下的文档查看，可能会导致短时间的页面空白 耐不住性子的或许早就关闭网页了，能看到这里的再次感谢

---


[![Cricut fonts](https://see.fontimg.com/api/renderfont4/pV3O/eyJyIjoiZnMiLCJoIjo4NywidyI6MTAwMCwiZnMiOjg3LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/Sm9raW5n/digidon-demo.png)](https://nidhoggdjoking.gitee.io/eternal/)

<span class="englist">Friends from other places, Please click on this name</span>

<div class="full-screen">
    全
</div>

<script>
// 年龄变化
let me = 1996
let nowYear = new Date().getFullYear()
let age = nowYear - me
$('#age').html(age);

// 手动开启全屏
var state = true;
var elem = document.documentElement;
function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE11 */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) { /* Safari */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE11 */
    document.msExitFullscreen();
  }
}

$('.full-screen').click(function(){ 
    if(state){
        openFullscreen();
        state = !state;
        $('.full-screen').text('缩');
    }else{
        closeFullscreen();
        state = !state;
        $('.full-screen').text('全');
    }
});

// 自动触发关闭侧边栏
$('.sidebar-toggle').trigger("click");
</script>


<style>
.phone{
    width:216px;
    height:480px;
    display:inline-block;
    margin:10px;
    /* 54  108 162 216 324*/
    /* 120  240  360 480 720*/
}

.englist{
    font-family: "Comic Sans MS", cursive, sans-serif;
    font-weight:600;
    font-size: 20px;
}
.full-screen{
    position: fixed;
    cursor: pointer;
    right: 3%;
    user-select: none;
    bottom: 3%;
    z-index: 10;
    background-color:#fff;
    width:60px;
    height:60px;
    border-radius:50%;
    text-align: center;
    line-height: 60px;
    font-size: 22px;
    color:#333;
    box-shadow:0 0 5px #9a9a9a;
}
</style>