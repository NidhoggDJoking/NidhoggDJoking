# CSS font-family 各字体一览表

---

> ### windows 内置字体


- <p class="family" style="font-family:SimSun">宋体 SimSun 012</p>

```css
font-family:SimSun;
```

- <p class="family" style="font-family:SimHei">黑体 SimHei 012</p>

```css
font-family:SimHei;
```

- <p class="family" style="font-family:Microsoft Yahei">微软雅黑 Microsoft Yahei 012</p>

```css
font-family:Microsoft Yahei;
```

- <p class="family" style="font-family:Microsoft JhengHei">微软正黑体 Microsoft JhengHei 012</p>

```css
font-family:Microsoft JhengHei;
```

- <p class="family" style="font-family:KaiTi">楷体 KaiTi 012</p>

```css
font-family:KaiTi;
```

- <p class="family" style="font-family:NSimSun">新宋体 NSimSun 012</p>

```css
font-family:NSimSun;
```

- <p class="family" style="font-family:FangSong">仿宋 FangSong 012</p>

```css
font-family:FangSong;
```

- <p class="family" style="Comic Sans MS', cursive">文生·康奈尔 Comic Sans 012</p>

```css
font-family: 'Comic Sans MS', cursive;
```

---

> ### 浏览器 内置字体

- <p class="family" style="font-family: -webkit-body;">无名氏 -webkit-body 012</p>

```css
font-family: -webkit-body;
```

- <p class="family" style="font-family: -webkit-pictograph;">无名氏 -webkit-pictograph 012</p>

```css
font-family: -webkit-pictograph;
```

- <p class="family" style="font-family: auto;">无名氏 auto 012</p>

```css
font-family: auto;
```

- <p class="family" style="font-family: cursive;">无名氏 cursive 012</p>

```css
font-family: cursive;
```

- <p class="family" style="font-family: fantasy;">无名氏 fantasy 012</p>

```css
font-family: fantasy;
```


- <p class="family" style="font-family: inherit;">无名氏 inherit 012</p>

```css
font-family: inherit;
```


- <p class="family" style="font-family: initial;">无名氏 initial 012</p>

```css
font-family: initial;
```


- <p class="family" style="font-family: monospace;">无名氏 monospace 012</p>

```css
font-family: monospace;
```

- <p class="family" style="font-family: none;">无名氏 none 012</p>

```css
font-family: none;
```


- <p class="family" style="font-family: sans-serif;">无名氏 sans-serif 012</p>

```css
font-family: sans-serif;
```


- <p class="family" style="font-family: serif;">无名氏 serif 012</p>

```css
font-family: serif;
```


- <p class="family" style="font-family: unset;">无名氏 unset 012</p>

```css
font-family: unset;
```

---

> ### Serif 字体

- <p class="family" style="font-family: Georgia, serif;">无名氏 Georgia, serif 012</p>

```css
font-family: Georgia, serif;
```

- <p class="family" style="font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif;">无名氏 "Palatino Linotype", "Book Antiqua", Palatino, serif 012</p>

```css
font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
```

- <p class="family" style="font-family: 'Times New Roman', Times, serif">无名氏 "Times New Roman", Times, serif 012</p>

```css
font-family: "Times New Roman", Times, serif;
```

---

> ### sans - serif字体

- <p class="family" style="font-family: Arial, Helvetica, sans-serif;">无名氏 Arial, Helvetica, sans-serif 012</p>

```css
font-family: Arial, Helvetica, sans-serif;
```

- <p class="family" style="font-family: Arial Black, Gadget, sans-serif;">无名氏 Arial Black, Gadget, sans-serif 012</p>

```css
font-family: Arial Black, Gadget, sans-serif;
```

- <p class="family" style="font-family: 'Comic Sans MS', cursive, sans-serif;">无名氏 "Comic Sans MS", cursive, sans-serif 012</p>

```css
font-family: "Comic Sans MS", cursive, sans-serif;
```

- <p class="family" style="font-family: Impact, Charcoal, sans-serif;">无名氏 Impact, Charcoal, sans-serif 012</p>

```css
font-family: Impact, Charcoal, sans-serif;
```

- <p class="family" style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">无名氏 "Lucida Sans Unicode", "Lucida Grande", sans-serif 012</p>

```css
font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
```

- <p class="family" style="font-family: Tahoma, Geneva, sans-serif;">无名氏 Tahoma, Geneva, sans-serif 012</p>

```css
font-family: Tahoma, Geneva, sans-serif;
```

- <p class="family" style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">无名氏 "Trebuchet MS", Helvetica, sans-serif 012</p>

```css
font-family: "Trebuchet MS", Helvetica, sans-serif;
```

- <p class="family" style="font-family: Verdana, Geneva, sans-serif;">无名氏 Verdana, Geneva, sans-serif 012</p>

```css
font-family: Verdana, Geneva, sans-serif;
```

---

> ### Monospace 字体

- <p class="family" style="font-family: 'Courier New', Courier, monospace;">无名氏 "Courier New", Courier, monospace 012</p>

```css
font-family: "Courier New", Courier, monospace;
```

- <p class="family" style="font-family: 'Lucida Console', Monaco, monospace;">无名氏 "Lucida Console", Monaco, monospace 012</p>

```css
font-family: "Lucida Console", Monaco, monospace;
```

---

[![Cricut fonts](https://see.fontimg.com/api/renderfont4/BW55G/eyJyIjoiZnMiLCJoIjo2NSwidyI6MTAwMCwiZnMiOjY1LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TmlkaG9nZ8K3RMK3Sm9raW5n/junitta.png)](https://www.fontspace.com/category/cricut)

[![Cricut fonts](https://see.fontimg.com/api/renderfont4/BW55G/eyJyIjoiZnMiLCJoIjo2NSwidyI6MTAwMCwiZnMiOjY1LCJmZ2MiOiIjRTgxMjcxIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/Sm9raW5n/junitta.png)](https://www.fontspace.com/category/cricut)


<style>
    .family{
        font-size:20px;
    }
</style>