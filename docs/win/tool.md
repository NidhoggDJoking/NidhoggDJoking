# windows


### window 地址


- ##### window.location.protocol 返回使用的 web 协议（http: 或 https:）
- ##### window.location.hostname 返回 web 主机的域名 'localhost'
- ##### window.location.host 返回 web 主机的域名 'localhost:3000'

- ##### window.location.href 返回当前页面的 href (URL) 'http://localhost:3000/#/win/tool'
- ##### window.location.pathname 返回当前页面的路径或文件名
- ##### window.location.assign 加载新文档