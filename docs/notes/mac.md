## MacBook pro 的快捷键

---

#### 常用的系统快捷键

```bash
command M:缩小窗口
command W:关闭窗口
command C:连接服务器
control 单击：鼠标右键
command ＋ H：隐藏窗口
command Q:退出应用程序
command Z:恢复上一步
command shift Z:恢复下一步
command shift F:前往 Finder;
command option:隐藏（打开）Dock;
command option W:关闭所有窗口
command option M:缩小所有窗口
command E:退出外接装置
command shift A:前往应用程序文件夹（在 finder 时）
command control option 8:全屏幕反显
command control option 弹出键：关机
command control 开关键：强制重开机
command option 弹出键：电脑休眠
command option esc：强制退出应用程序
command ＋单按标题列：跳到上一页
command delete:删除文件
command option shift delete:强制清除废纸篓
command option D:打开内置字典，单字实时翻译
command .（句号）：终止任何进行中的操作
command down：在任何文件上使用，可以直接打开该文件
fn delete:删除光标右边的字
command ＋ esc:启动 Front Row 窗口操作。
```

#### 窗口操作

```
option ＋单击窗口关闭格（红色）：关闭桌面上所有窗口
option ＋单击窗口缩放格（绿色）：可将窗口调整至与屏幕一样大
option ＋单击窗口收缩格（黄色）：将所有窗口收拢
command W:关闭单一窗口
command N:打开新窗口
command option W:关闭所有窗口
command 窗口名称栏：可弹出窗口路径
command option 窗口名称栏：可返回路径窗口并关掉目前窗口。
Control+Cmmand+F 就可以实现网页全屏
```

#### Finder

```
command 1:使用 icon 阅览
command 2:使用清单预览
command 3:使用专栏预览
command 4:使用 CoverFlow 预览
command Y:使用 Quick Look 预览
command option Y:使用 Quick Look 全屏幕预览
command shift H:打开 Home 目录
command shift I:打开 iDisk
command shift D:打开桌面
command shift C:打开电脑
command shift K:打开网络
command shift A:打开应用程序
option 双击：打开程序自动关闭 Finder 窗口
特殊效果：command shift option down.
```

#### 辅助功能

```
command option 8:启动／停用缩放功能
command option =:放大
command option -:缩小
command option control ,:降低对比
command option control .:调高对比
command option control 8:X 反光效果。
```

#### 开机相关命令快捷键

```
开机时按住 C 键不放光盘开机
开机时按住 N 键不放可用网络开机
开机时按住 T 键不放可用硬盘开机
开始时按 option 键选择开机磁盘
开机时按 command option shift delete 组合键以外接储存装置内的系统开机
开机时按 shift 键不放可用安全模式开机
开机时按 V 键不放可进入 Verbose 模式
开机时按 S 键不放可进入 Single-User 单人模式
开机时按住鼠标（左）键可将光盘强制退出
开机时按 command option O F 进入 Open Firmware 模式
```