> ### 相关内容阐述：

- ### WebGL:

?> WebGL（全写`Web Graphics Library`）是一种3D绘图协议，这种绘图技术标准允许把`JavaScript`和`OpenGL ES 2.0`结合在一起，通过增加`OpenGL ES 2.0`的一个`JavaScript`绑定，WebGL可以为HTML5 `Canvas`提供硬件3D加速渲染，这样Web开发人员就可以借助系统显卡来在浏览器里更流畅地展示3D场景和模型了，还能创建复杂的导航和数据视觉化。显然，WebGL技术标准免去了开发网页专用渲染插件的麻烦，可被用于创建具有复杂3D结构的网站页面，甚至可以用来设计3D网页游戏等等。

-  ### OpenGL:

?> `OpenGL`（英语：`Open Graphics Library`，译名：开放图形库或者“开放式图形库”）是用于渲染2D、3D矢量图形的跨语言、跨平台的应用程序编程接口（API）。这个接口由近350个不同的函数调用组成，用来绘制从简单的图形比特到复杂的三维景象。而另一种程序接口系统是仅用于`Microsoft Windows`上的`Direct3D`。`OpenGL`常用于CAD、虚拟现实、科学可视化程序和电子游戏开发。`OpenGL`的高效实现（利用了图形加速硬件）存在于`Windows`，部分`UNIX平台`和Mac OS。这些实现一般由显示设备厂商提供，而且非常依赖于该厂商提供的硬件。开放源代码库Mesa是一个纯基于软件的图形API，它的代码兼容于`OpenGL`。但是，由于许可证的原因，它只声称是一个“非常相似”的API。

-  ### Three.js:

?> `Three.js`是基于原生`WebGL`封装运行的三维引擎，即`three.js`是`JavaScript`编写的WebGL第三方库