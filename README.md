<p align="center">
    <img width="240" src="https://nidhoggdjoking.gitee.io/storage/img/pikmi9.png" alt="logo"/>
</p>


<h1 align="center">Joking Blog v3.0</h1>


### 安装：

```sh
npm i docsify-cli -g
```

### 初始化：

```sh
docsify init ./docs
```


### 运行：

```sh
docsify serve docs
```


  [ 📌 docsify 文档](https://docsify.js.org/#/)



### serviceWorker

 [Progressive Web Apps](https://developers.google.com/web/progressive-web-apps/)(PWA) 是一项融合 Web 和 Native 应用各项优点的解决方案。我们可以利用其支持离线功能的特点，让我们的网站可以在信号差或者离线状态下正常运行。 要使用它也非常容易。 

 ### 新的代码高亮方案


1 [highlightjs](https://highlightjs.org/)

2 [prismjs](https://prismjs.com/)

上述2个的包CND极不稳定 暂且搁置